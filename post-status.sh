#!/bin/bash

URL="https://mastodon.social/"
API="api/v1/statuses"
ACCESS_TOKEN=$(pass show mastodon/access-token)
STATUS_POST="Updating status via Mastodon's API."

curl "${URL}${API}" \
    -H "Authorization: Bearer $ACCESS_TOKEN" \
    -F "status=$STATUS_POST"


