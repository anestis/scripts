#!/bin/bash

SCRIPT=$(basename $0)
VERSION="v1.0.1"

TEMPLATE=""
PROVIDER=""
MODULE=""
BACKEND=""
USE_GIT=0

OPTIONS="b:p:m:hvg"
LONG_OPTIONS="new:,backend:,git,version,provider:,module:,help"
PARSED_OPTIONS=$(getopt -o $OPTIONS --long $LONG_OPTIONS -n "$0" -- "$@")

if [ $? -ne 0 ]; then
    exit 1
fi

eval set -- "$PARSED_OPTIONS"

usage() {
    echo "$SCRIPT is a script to create a simple Terraform project"
    echo
    echo "Usage:"
    echo "  $SCRIPT [flags]"
    echo "  $SCRIPT [commands]" 
    echo
    echo "Available commands:"
    echo "  new         Create a new Terraform project"
    echo "  version     Print script version"
    echo
    echo "Available flags:"
    echo "  -g, --git                   Enable Git versioning"
    echo "  -p, --provider string       Include provider"
    echo "  -m, --module string         Initialize module directory"
    echo "  -b, --backend string        Initialize Terraform backend"
    echo "  -h, --help                  Print usage"    

    exit 0
}

print_version() {
    echo $VERSION
    exit 0
}

if [ "$#" -eq 0 ]; then
    usage
fi

while true; do
    case "$1" in
        --new)
            TEMPLATE="$2"
            shift 2
            ;;
        -p|--provider)
            PROVIDER="$2"
            shift 2
            ;;
        -m|--module)
            MODULE="$2"
            shift 2
            ;;
        -b|--backend)
            BACKEND="$2"
            shift 2
            ;;
        -g|--git)
            USE_GIT=1
            shift
            ;;
        -v|--version)
            print_version
            ;;
        -h|--help)
            usage
            ;;         
        --)
            shift
            break
            ;;
        *)
            echo "Invalid option: $1"
            usage
            ;;
    esac
done


if [ -n "$TEMPLATE" ]; then
    mkdir "$TEMPLATE" && cd "$TEMPLATE"
    touch terraform.tf main.tf README.md variables.tf outputs.tf LICENSE terraform.tfvars

    if [ -n "$BACKEND" ]; then
        echo -e "terraform {\n    backend $BACKEND {\n    }\n}" > terraform.tf
    fi

    if [ -n "$MODULE" ]; then
        mkdir -p modules/"$MODULE"
        touch modules/"$MODULE"/main.tf modules/"$MODULE"/variables.tf modules/"$MODULE"/outputs.tf
    fi

    if [ -n "$PROVIDER" ]; then
        echo "provider \"$PROVIDER\" {}" >> main.tf
    fi

    if [ "$USE_GIT" -eq 1 ]; then
        echo -e "terraform/\n*tfvars" > .gitignore
        git init &> /dev/null
        git add . &> /dev/null
        git commit -m "Initial commit" &> /dev/null
    fi
    echo "Terraform project '$TEMPLATE' has been initialized."
#cat << EOF > README.md
#This is a brief description of the Terraform module
#EOF
    
fi
