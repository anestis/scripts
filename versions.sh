#!/bin/bash

# print versions of most important dev tools installed

declare -A versions

if [[ -x `which bash 2> /dev/null` ]]; then
	BASH_V=$(bash --version | grep 'GNU bash' | cut -d' ' -f4 | cut -c1-5)
	versions[bash]=$BASH_V
fi

if [[ -x `which python3 2> /dev/null` ]]; then
	PYTHON3_V=$(python3 --version | cut -d' ' -f2)
	versions[python3]=$PYTHON3_V
fi

if [[ -x `which perl 2> /dev/null` ]]; then
	PERL_V=$(perl -v | grep version | cut -d' ' -f9 | tr -d 'v()')
	versions[perl]=$PERL_V
fi

if [[ -x `which awk` ]]; then
	AWK_V=$(awk --version | grep 'GNU Awk' | cut -d' ' -f3 | tr -d ,)
	versions[awk]=$AWK_V
fi

if [[ -x `which ruby` ]]; then
	RUBY_V=$(ruby -v | grep ruby | cut -d' ' -f2 | cut -c 1-5)
	versions[ruby]=$RUBY_V
fi

if [[ -x `which gcc` ]]; then
	GCC_V=$(gcc --version | grep gcc | cut -d' ' -f4)
	versions[gcc]=$GCC_V
fi

if [[ -x `which clang 2> /dev/null` ]]; then
	CLANG_V=$(clang --version | grep version | cut -d' ' -f3)
	versions[clang]=$CLANG_V
else
	versions[clang]="NA"
fi

if [[ -x `which php 2> /dev/null` ]]; then
	PHP_V=$(php -v | grep cli | cut -d ' ' -f2)
	versions[php]=$PHP_V
else
	versions[php]="NA"
fi

if [[ -x `which vim 2> /dev/null` ]]; then
	VIM_V=$(vim --version | grep 'Vi IMproved' | cut -d' ' -f 5)
	versions[vim]=$VIM_V
else
	versions[vim]="NA"
fi

#if [[ -x `which java 2> /dev/null` ]]; then
#	JAVA_V=$(java -version | grep version | cut -d' ' -f3 | tr -d \")
#	versions[openjdk]=$JAVA_V
#fi

if [[ -x `which rustc 2> /dev/null` ]]; then
	RUSTC_V=$(rustc --version | cut -d ' ' -f2)
	versions[rustc]=$RUSTC_V
else
	versions[rustc]="NA"
fi

# present results

printf " %-10s    %10s\n" "Program" "Version"
printf "+------------------------+\n" 

for program in "${!versions[@]}"; do
	printf " %-10s -- %10s\n" $program ${versions[$program]}
done
